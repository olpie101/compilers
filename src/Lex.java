import grammar.lexer.Lexer;
import grammar.lexer.LexerException;
import grammar.node.EOF;
import grammar.node.Token;
import grammar.parser.ParserException;

import java.io.*;

public class Lex {

    public static void main(String[] args) throws IOException, LexerException, ParserException {
        Lexer l = new Lexer(new PushbackReader(new BufferedReader(new FileReader(args[0]))));
        Token token;
        StringBuilder outputData = new StringBuilder();
        while(!((token = l.next()) instanceof EOF)){
            String tokenOutput;
            switch(token.getClass().getSimpleName()){
                //These require just printing out the token name
                case "TComment":
                case "TFunc":
                case "TWhitesapce":
                    outputData.append(token.getClass().getSimpleName().substring(1).toUpperCase()+"\n");
                    break;
                //These require just printing out the token value
                case "TPlus":
                case "TMinus":
                case "TDiv":
                case "TMult":
                case "TEquals":
                case "TLPar":
                case "TLBrace":
                case "TRPar":
                case "TRBrace":
                    outputData.append(token.getText()+"\n");
                    break;
                //These require just printing out the token name and value
                case "TId":
                    outputData.append(token.getClass().getSimpleName().substring(1).toUpperCase()+","+token.getText()+"\n");
                    break;
                case "TFloatLiteral":
                    outputData.append("FLOAT_LITERAL"+","+token.getText()+"\n");
                    break;
            }
        }
        //Save output to file and print output
        String outputFileName = args[0].substring(0, args[0].indexOf('.'))+".tkn";
        BufferedWriter bw = new BufferedWriter(new FileWriter(new File(outputFileName)));
        bw.write(outputData.toString().trim()+"\n");
        bw.close();
        System.out.println(outputData.toString().trim());
    }
}
