import grammar2.node.Node;

/**
 * Created by eduardokolomajr on 2014/09/15.
 */
public class TokenIdentifier {
    public static String eval(Node token){
        //System.out.println("##"+token.getClass().getSimpleName());
        //return token.getClass().getSimpleName();

        switch (token.getClass().getSimpleName()){
            //These require just printing out the token name
            case "AProgram":
            case "AAssignStatement":
            case "ADivExp":
            case "AFloatLitExp":
            case "AIdExp":
            case "AMinusExp":
            case "AMultExp":
            case "APlusExp":
                return token.getClass().getSimpleName().substring(1)+"\n";
            //These require just printing out the token name and value
            case "TId":
                //System.out.println("^^^Id found\n");
                //System.out.println("^^^"+token.getClass().getSimpleName().substring(1).toUpperCase()+","+token.toString()+"\n");
                return token.getClass().getSimpleName().substring(1).toUpperCase()+","+token.toString()+"\n";
            case "TFloatLiteral":
                return "FLOAT_LITERAL"+","+token.toString()+"\n";
            case "EOF":
                return "";
        }
        return token.getClass().getSimpleName()+"\n";
    }
}
