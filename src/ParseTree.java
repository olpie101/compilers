import grammar.analysis.DepthFirstAdapter;
import grammar.node.*;

/**
 * Created by eduardokolomajr on 2014/09/11.
 */
public class ParseTree extends DepthFirstAdapter {
    StringBuilder outputData;       // final output
    int depth;                      // track how far to indent
    public static String eval(Node start){
        ParseTree tree = new ParseTree();
        start.apply(tree);
        return tree.getValue();
    }

    private ParseTree (){
        depth = 0;
        outputData = new StringBuilder();
    }

    /**
     * Return the output sitting in the parse tree
     * @return output data
     */
    public String getValue(){
        return outputData.toString();
    }

    /**
     * used to repeat a string a number of times
     * @param input string to repeat
     * @param factor number of times to repeat string
     * @return input repeated by factor
     */
    private String stringRepeat(String input, int factor) {
        StringBuilder output = new StringBuilder();
        for (int i=0; i<factor; ++i){
            output.append(input);
        }
        return output.toString();
    }

    /**
     * All methods with 'in' increase depth by and all out methods would
     * need to decrease by one.
     * @param node
     */
    @Override
    public void defaultOut(@SuppressWarnings("unused") Node node) {
        --depth;
    }


    @Override
    public void inStart(Start node) {
        outputData.append(stringRepeat("\t", depth)+"Start\n");
        ++depth;
    }

    @Override
    public void inAProgram(AProgram node) {
        outputData.append(stringRepeat("\t", depth)+"Program\n");
        ++depth;
    }

    @Override
    public void inAFunctionDecl(AFunctionDecl node) {
        outputData.append(stringRepeat("\t", depth)+"Functiondecl\n");
        ++depth;
    }

    @Override
    public void inAFunctionHead(AFunctionHead node) {
        outputData.append(stringRepeat("\t", depth)+"Functionhead\n");
        ++depth;
    }

    @Override
    public void inAFunctionBody(AFunctionBody node) {
        outputData.append(stringRepeat("\t", depth)+"Functionbody\n");
        ++depth;
    }

    @Override
    public void inAStatement(AStatement node) {
        outputData.append(stringRepeat("\t", depth)+"Statement\n");
        ++depth;
    }

    @Override
    public void inAAddExpression(AAddExpression node) {
        outputData.append(stringRepeat("\t", depth)+node.getExpression()+"\n");
        ++depth;
    }

    @Override
    public void inAMinusExpression(AMinusExpression node) {
        outputData.append(stringRepeat("\t", depth)+node.getExpression()+"\n");
        ++depth;
    }

    @Override
    public void inATermExpression(ATermExpression node) {
        outputData.append(stringRepeat("\t", depth)+"CasetermExpression\n");
        ++depth;
    }

    @Override
    public void inAMultiplyTerm(AMultiplyTerm node) {
        outputData.append(stringRepeat("\t", depth)+"CasemultipyTerm\n");
        ++depth;
    }

    @Override
    public void inADivideTerm(ADivideTerm node) {
        outputData.append(stringRepeat("\t", depth)+"CasedivideTerm\n");
        ++depth;
    }

    @Override
    public void inAFactorTerm(AFactorTerm node) {
        outputData.append(stringRepeat("\t", depth)+"CasefactorTerm\n");
        ++depth;
    }

    @Override
    public void inAFloatLitFactor(AFloatLitFactor node) {
        outputData.append(stringRepeat("\t", depth)+"CasefloatFactor\n");
        ++depth;
    }

    @Override
    public void inAFuncFactor(AFuncFactor node) {
        outputData.append(stringRepeat("\t", depth)+"CasefuncFactor\n");
        ++depth;
    }

    @Override
    public void inAIdFactor(AIdFactor node) {
        outputData.append(stringRepeat("\t", depth)+"CaseidFactor\n");
        ++depth;
    }

    @Override
    public void inAExprFactor(AExprFactor node) {
        outputData.append(stringRepeat("\t", depth)+"CaseexpressionFactor\n");
        ++depth;
    }

    @Override
    public void caseTFunc(TFunc node) {
        outputData.append(stringRepeat("\t", depth) + "FUNC\n");
    }

    @Override
    public void caseTId(TId node) {
        outputData.append(stringRepeat("\t", depth) + "ID," + node.getText() + "\n");
    }

    @Override
    public void caseTFloatLiteral(TFloatLiteral node) {
        outputData.append(stringRepeat("\t", depth) + "FLOAT_LITERAL," + node.getText() + "\n");
    }

    //append node value for the remaining cases

    @Override
    public void caseTLPar(TLPar node) {
        outputData.append(stringRepeat("\t", depth) +node.getText() + "\n");
    }

    @Override
    public void caseTRPar(TRPar node) {
        outputData.append(stringRepeat("\t", depth) + node.getText() + "\n");
    }

    @Override
    public void caseTLBrace(TLBrace node) {
        outputData.append(stringRepeat("\t", depth) + node.getText() + "\n");
    }

    @Override
    public void caseTRBrace(TRBrace node) {
        outputData.append(stringRepeat("\t", depth) + node.getText() + "\n");
    }

    @Override
    public void caseTMinus(TMinus node) {
        outputData.append(stringRepeat("\t", depth) + node.getText() + "\n");
    }

    @Override
    public void caseTEquals(TEquals node) {
        outputData.append(stringRepeat("\t", depth) + node.getText() + "\n");
    }

    @Override
    public void caseTPlus(TPlus node) {
        outputData.append(stringRepeat("\t", depth) + node.getText() + "\n");
    }

    @Override
    public void caseTDiv(TDiv node) {
        outputData.append(stringRepeat("\t", depth) + node.getText() + "\n");
    }

    @Override
    public void caseTMult(TMult node) {
        outputData.append(stringRepeat("\t", depth) + node.getText() + "\n");
    }

    @Override
    public void caseTWhitespace(TWhitespace node) {
        outputData.append(stringRepeat("\t", depth) + node.getText() + "\n");
    }
}