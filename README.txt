Author: Eduardo Koloma Junior (KLMEDU001)
Date:	14 September 2014

Name:	Compilers 1

Description:	Program creates compiler using SableCC and allows the user to perform a lexical analysis or to parse the input program a produce a tree.

Instructions:
1) Open the terminal and run the make command in the ‘src’ folder which can be in the root of the folder where all the provided files are contained. This command will create the necessary files from the grammar file and compile the whole program.

2) Either run ‘java Lex <inputFileName>’ for lexical analysis or ‘java Parse <inputFileName’ for the parse tree traversal. The ‘program.nul’ file has been provided as example input.

3) After the program has ran, it shall produce output to the console as well as create an output file, ‘.tkn’ in the case of the lexical analysis and a ‘.str’ for the parseTree traversal.