Package grammar;

Helpers
    space = ' ';
    tab = 9;
    cr = 13;
    lf = 10;
    all = [0 .. 0xFFFF];
    digit = ['0'..'9'];
    lowercase = ['a'..'z'];
    uppercase = ['A'..'Z'];
    letter  = lowercase | uppercase;
    underscore = '_';
    idletter = letter | underscore;
    idchar  = idletter | digit;
    eol   = cr | lf | cr lf;
    all_not_cr_lf = [all - [cr + lf]];
    decimal = '.' digit+;
    exponent = ('e' |'E')('+' | '-')? digit+;

Tokens
    func = 'func';
    whitespace = (space | tab | eol)+;
    mult  = '*';
    div = '/';
    plus  = '+';
    minus = '-';
    equals = '=';
    l_par = '(';
    r_par = ')';
    l_brace = '{';
    r_brace = '}';
    float_literal = ('-')? digit+ decimal? exponent?;
    id = idletter idchar*;
    comment = ('//' [all - [cr + lf]]* eol?) |('/*' [all-'*']* '*'+([[all-'*']-'/'] [all-'*']* '*'+)* '/');

Ignored Tokens
    whitespace, comment;

Productions
    program = function_decl* statement*;                     // these asterisks indicate closure
    
    function_decl = function_head function_body;
    
    function_head = func [name]:id  l_par [parameter]:id r_par;
    
    function_body = l_brace statement* r_brace;             // this asterisk indicates closure
    
    statement = id equals expression;
    
    expression = {add} expression plus term |
                    {minus} expression minus term |
                    {term} term;
    
    term = {multiply} term mult factor |                    // this asterisk indicates multiplication
            {divide} term  div factor|
            {factor} factor;
    
    factor = {expr} l_par expression r_par |
                {func} id l_par expression r_par |
                {float_lit} float_literal |
                {id} id;
