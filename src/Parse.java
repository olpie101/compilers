import grammar2.lexer.Lexer;
import grammar2.lexer.LexerException;
import grammar2.node.Start;
import grammar2.parser.Parser;
import grammar2.parser.ParserException;

import java.io.*;

/**
 * Created by eduardokolomajr on 2014/09/11.
 */
public class Parse {
    public static void main (String args[]) throws IOException{
        Lexer l = new Lexer(new PushbackReader(new BufferedReader(new FileReader(args[0]))));

        Parser p = new Parser(l);
        String outputData = "";
        try {
            Start start = p.parse();
            outputData = ParseTreeMini.eval(start);
        }catch (LexerException le){
//            le.printStackTrace();
            System.out.println(le.getClass().getName()+": "+le.getLocalizedMessage());
        }catch (ParserException pe) {
//            pe.printStackTrace();
            System.out.println(pe.getClass().getName()+": "+pe.getLocalizedMessage());
        }

        //Save output to file and print output
//        String outputFileName = args[0].substring(0, args[0].indexOf('.'))+".str";
//        BufferedWriter bw = new BufferedWriter(new FileWriter(new File(outputFileName)));
//        bw.write(outputData.trim()+"\n");
//        bw.close();
        System.out.println(outputData.toString().trim());
    }
}
