import grammar2.node.Token;

/**
 * Created by eduardokolomajr on 2014/09/15.
 */
public class SemanticException extends Exception {
    private Token token;

    public SemanticException(@SuppressWarnings("hiding") Token token, String  message)
    {
        super(message);
        this.token = token;
    }

    public Token getToken()
    {
        return this.token;
    }
}
