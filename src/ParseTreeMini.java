import grammar2.analysis.DepthFirstAdapter;
import grammar2.node.AAssignStatement;
import grammar2.node.Node;
import grammar2.node.TId;
import grammar2.node.Token;

import java.util.HashMap;

/**
 * Created by eduardokolomajr on 2014/09/15.
 */
public class ParseTreeMini extends DepthFirstAdapter {
    StringBuilder outputData;       // final output
    int depth;                      // track how far to indent
    boolean insideAssignment = false;
    boolean insideID = false;
    boolean completeAssignmentStage1 = false;
    HashMap<String, String> idTable;
    public static String eval(Node start){
        ParseTreeMini tree = new ParseTreeMini();
        start.apply(tree);
        return tree.getValue();
    }

    private ParseTreeMini (){
        depth = 0;
        outputData = new StringBuilder();
        idTable = new HashMap<>();
    }

    /**
     * Return the output sitting in the parse tree
     * @return output data
     */
    public String getValue(){
        return outputData.toString();
    }

    /**
     * used to repeat a string a number of times
     * @param input string to repeat
     * @param factor number of times to repeat string
     * @return input repeated by factor
     */
    private String stringRepeat(String input, int factor) {
        StringBuilder output = new StringBuilder();
        for (int i=0; i<factor; ++i){
            output.append(input);
        }
        return output.toString();
    }

    /**
     * All methods with 'in' increase depth by and all out methods would
     * need to decrease by one.
     * @param node
     */
    @Override
    public void defaultOut(@SuppressWarnings("unused") Node node)
    {
        if(node instanceof AAssignStatement) {
            insideAssignment = false;
            completeAssignmentStage1 = false;
        }
        --depth;
    }


    @Override
    public void defaultIn(@SuppressWarnings("unused") Node node) {
        if(node instanceof AAssignStatement)
            insideAssignment = true;
        String toAppend = TokenIdentifier.eval(node);

        outputData.append(stringRepeat("\t", depth)+TokenIdentifier.eval(node));

        ++depth;
    }

    @Override
    public void defaultCase(@SuppressWarnings("unused") Node node) {
        if(insideAssignment){
//            System.out.println("IA =>"+node.getClass().getSimpleName()+","+node.toString());
            try {
                if(node instanceof TId)
                    checkId(node);
                if(!completeAssignmentStage1)
                    completeAssignmentStage1 = true;
            } catch (SemanticException e) {
                throw new RuntimeException(e);
            }
        }
        outputData.append(stringRepeat("\t", depth)+TokenIdentifier.eval(node));
    }

    boolean checkId(Node node) throws SemanticException{
        if(insideAssignment && completeAssignmentStage1){
            if(!idTable.containsKey(node.toString()))
                throw new SemanticException((Token)node, node.toString()+" not defined.");
        }else if(insideAssignment) {
            if (idTable.containsKey(node.toString())) {
                throw new SemanticException((Token) node, node.toString() + " already defined.");
            } else {
                idTable.put(node.toString(), "");
            }
        }
//        System.out.println("CIDE =>"+node.getClass().getSimpleName()+","+node.toString());
        return true;
    }
}
